$('.explore-carousel').slick({
    infinite: false,
    slidesToShow: 1,
    centerMode: false,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 900,
            settings: {
                draggable: true
            }
        },
        {
            breakpoint: 1800,
            settings: {
                draggable: false
            }
        }
    ]
  });
$('.recommended').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    prevArrow: $('.recommended-cover .prev'),
    nextArrow: $('.recommended-cover .next'),
  });

  var currentSlide = $('.recommended').slick('slickCurrentSlide');
  $('.recommended-cover .prev').toggle(currentSlide != 0);
  $('.recommended-cover .next').toggle(currentSlide != 4);

  $('.recommended').on('afterChange', function(){
      $('.recommended-cover .prev,.recommended-cover .next').show();
  });

$(".rate-yo").rateYo({
    rating: 4,
    starWidth: "10px",
    halfStar: true,
    spacing: "1px",
    ratedFill: '#08898d',
    readOnly: true
});
